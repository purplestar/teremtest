export const dashboardConstants = {
    CAROUSEL_LIST_REQUEST: 'CAROUSEL_LIST_REQUEST',
    CAROUSEL_LIST_SUCCESS: 'CAROUSEL_LIST_SUCCESS',
    CAROUSEL_LIST_FAILURE: 'CAROUSEL_LIST_FAILURE',
};