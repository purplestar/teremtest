import React, { useState, useEffect } from 'react'
import './Dashboard.css'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router'
import * as dashboardActions from '../actions/Dashboard.actions'
import { useSelector } from 'react-redux'
import Carousel from './carousel/Carousel'

function Dashboard(props) {
  const [carouselResults, setCarouselResults] = useState([])
  const [featuredResults, setFeaturedResults] = useState([])

  useEffect(() => {
    const getResults = () => {
      props.loadCarouselAndFeature()
    };
    getResults()
  }, [])

  const response = useSelector(state => state.dashboard)
  //console.log(response)

  if (carouselResults.length === 0
    && response.isLoading === false) {
    setCarouselResults(response[0].data.data)
    setFeaturedResults(response[1].data.data)
  }

  console.log(featuredResults)


  if (response.isLoading) {
    return (
      <div className="d-flex justify-content-center mt-5">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    )
  }
  else {
    return (
      <div className="container">
        <input type="text" className="form-control" id="formGroupExampleInput" placeholder="Search for..." />
        <div className="form-group">
          <label>Popular around you</label>
          <Carousel
            show={4}
          >
            {
              carouselResults.map((aResult, index) => (
                <div key={index} className="card">
                  <img className="card-img-top" src={aResult.img} alt="Card image cap" />
                  <div className="card-body">
                    <h5 className="card-title">{aResult.title}</h5>
                    <p className="card-text">{aResult.location}</p>
                  </div>
                </div>
              ))}
          </Carousel>
        </div>

        <div className="row">
          {
            featuredResults.map((aResult, index) => (
              <div key={index} className={`col-${12 / featuredResults.length}`}>
                <div className="card">
                  <img className="card-img-top" src={aResult.img} alt="Card image cap" />
                  <div className="card-body">
                    <h5 className="card-title">{aResult.title}</h5>
                    <p className="card-text">{aResult.location}</p>
                  </div>
                </div>
              </div>
            ))}
        </div>

      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    dashboard: state.dashboard
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...dashboardActions
    }, dispatch);
}

const connectedDashboard = withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard))
export default connectedDashboard;