import { dashboardConstants } from '../constants/Dashboard.constants'
import { dashboardServices } from '../services/Dashboard.services'

export function loadCarouselAndFeature() {
    return dispatch => {
        let isLoading = true
        dispatch(request({ isLoading }));

        dashboardServices.loadCarouselAndFeature()
            .then(
                dashboard => {
                    dispatch(success(dashboard))
                },
                error => {
                    dispatch(failure(error.toString()));
                    //dispatch(alertActions.error(error.toString()));
                }
            )
            .finally(
                () => {
                    isLoading = false
                    dispatch(request({ isLoading }))
                }
            )
    }

    function request(dashboard) { return { type: dashboardConstants.CAROUSEL_LIST_REQUEST, dashboard } }
    function success(dashboard) { return { type: dashboardConstants.CAROUSEL_LIST_SUCCESS, dashboard } }
    function failure(error) { return { type: dashboardConstants.CAROUSEL_LIST_FAILURE, error } }
}