import axios from 'axios'

export const dashboardServices = {
    loadCarousel,
    loadFeature,
    loadCarouselAndFeature
};

function loadCarousel() {
    return axios.get(`${process.env.REACT_APP_DOMAIN_NAME}/carousel`)
}

function loadFeature() {
    return axios.get(`${process.env.REACT_APP_DOMAIN_NAME}/featured`)
}

function loadCarouselAndFeature() {
    return Promise.all([loadCarousel(), loadFeature()])
}