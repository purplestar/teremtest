import React from "react";
import { Link, Route, Switch } from "react-router-dom";
import Dashboard from './components/Dashboard'



export default function App() {
  return (
    <div>
      { /* Route components are rendered if the path prop matches the current URL */}
      <Route exact path="/" component={Dashboard} />
    </div>
  );
}